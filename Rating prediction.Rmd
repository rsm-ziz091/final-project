---
title: "Predict rating based on users reviews"
author: "Chi Nguyen"
date: "5/17/2019"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
library(dplyr)
library(ggplot2)
library(readr)
library(tidytext)
library(tidyr)
library(stringr)
library(sjmisc)
library(stringdist)
library(tm)
## read data
reviews <- read.csv("employee_reviews.csv")
reviews <- as.data.frame(reviews)
```

#Join 2 columns: pros and cons
```{r}
reviews$review <- paste( reviews$pros, reviews$cons)

reviews$overall.ratings <- as.factor(reviews$overall.ratings) #change rating column to factor 

reviewsTidy <- reviews %>%
  select(X,review) %>%
  unnest_tokens(word,review) 

reviewsLength <- reviewsTidy %>%
  count(X) %>%
  rename(reviewLength = n)
  
sentRev <- reviewsTidy %>%
  inner_join(get_sentiments("afinn")) %>%
  group_by(X) %>%
  summarize(sentiment = sum(score)) %>%
  left_join(reviewsLength,by='X') %>%
  mutate(aveSentiment = sentiment/reviewLength)

sentByStar <- sentRev %>%
  left_join(select(reviews,X,overall.ratings), by='X')

```



```{r}
library(keras)

#number of positive reviews: 45688. 21841 negative reviews (1-3 star)
reviews %>% summarise(c = sum(overall.ratings >3))

set.seed(1234)

reviews_sample <- reviews %>% mutate(isPos = as.integer(overall.ratings > 3)) %>%
  group_by(isPos) %>% sample_n(21841)

nReviews <- nrow(reviews_sample)
```

```{r}
#map data to sequences - easier for Keras to work with 
maxWords <- 5000  #only use the top 3000 words 
tokenizer <- text_tokenizer(num_words = maxWords) %>%
  fit_text_tokenizer(reviews_sample$review)

sequences <- texts_to_sequences(tokenizer, reviews_sample$review)

word_index <- tokenizer$word_index

## one-hot code tokens and reshuffle data
x <- sequences_to_matrix(tokenizer, sequences, mode = c("binary"))
y <- as.numeric(reviews_sample$isPos)

nTrain <- 3000
shuffIndex <- sample(1:nReviews)
trainIndex <- shuffIndex[1:nTrain]
testIndex <- shuffIndex[(nTrain+1):nReviews]

xTrain <- x[trainIndex,]
yTrain <- y[trainIndex]

xTest <- x[testIndex,]
yTest <- y[testIndex]
```


```{r}
## model architecture 
model <- keras_model_sequential() %>% 
  layer_dense(units = 16, activation = "relu", input_shape = c(maxWords)) %>% 
  layer_dense(units = 16, activation = "relu") %>% 
  layer_dense(units = 1, activation = "sigmoid")

## compile model 
model %>% compile(
  optimizer = "rmsprop",
  loss = "binary_crossentropy",
  metrics = c("accuracy")
)

## @knitr trainModel

## fit 
historyT <- model %>% fit(
  xTrain,
  yTrain,
  epochs = 20,
  batch_size = 256,
  validation_split = 0.2
)

## re-train at optimized epoch
theEpoch=which.min(historyT$metrics$val_loss)

model <- keras_model_sequential() %>% 
  layer_dense(units = 16, activation = "relu", input_shape = c(maxWords)) %>% 
  layer_dense(units = 16, activation = "relu") %>% 
  layer_dense(units = 1, activation = "sigmoid")

model %>% compile(
  optimizer = "rmsprop",
  loss = "binary_crossentropy",
  metrics = c("accuracy")
)

history <- model %>% fit(xTrain, yTrain,epochs = theEpoch,batch_size = 256)

## @knitr predictModel 

results <- model %>% evaluate(xTest, yTest)
results

## @knitr specificPredictions

theTestIndices <- c(31,35)
cat(str_wrap(reviews$reviewText[testIndex[theTestIndices[1]]],width=60))
cat(str_wrap(reviews$reviewText[testIndex[theTestIndices[2]]],width=60))

model %>% predict(xTest[theTestIndices,])



```


## Random Forest model

DATA PREPARATION

1. Creat a word vector for description.

2. Build a corpus using the word vector.

3. Pre-processing tasks such as removing number, whitespaces, stopwords and conversion to lower case.

4. Build a document term matrix (dtm).

5. Remove sparse words from the above dtm.

6. The above step leads to a count frequency matrix showing the frequency of each word in its coressponding column.

7. Tranform count frequency matrix to a binary instance matrix, which shows occurences of a word in a document as either 0 or 1, 1 for being present and 0 for absent.

8. Append the label column from the original notes dataset with the transformed dtm. The label column has 5 labels ( 1-5 rating stars).

MODEL BUILDING

1. Randomly sample the dtm and split it into a traning set and testing set.

2. Build a base model of random forest with repeated 10-fold cross validation.

3. Check for accuracy of the model on the training set and testing set.


```{r}
table(reviews$overall.ratings)
```

**Word vector and Text pre-preprocessing**

```{r}
sd <- VectorSource(reviews$review)

corpus <- Corpus(sd)  # build corpus

corpus <- tm_map(corpus, removeNumbers)  # remove numbers

corpus <- tm_map(corpus, removePunctuation) # remove puntucations

corpus <- tm_map(corpus, stripWhitespace) # remove  white spaces

corpus <- tm_map(corpus, removeWords, stopwords('english')) # remove stopwords

corpus <- tm_map(corpus, content_transformer(tolower))  # change to lower case
```

**Building Document term matrix**

```{r}
tdm <- DocumentTermMatrix(corpus)  # build document term matrix

tdm_sparse <- removeSparseTerms(tdm, 0.90) # remove sparse terms

tdm_dm <- as.data.frame(as.matrix(tdm_sparse)) # count matrix

tdm_df <- as.matrix((tdm_dm > 0) + 0) # binary instance matrix

tdm_df <- as.data.frame(tdm_df)

tdm_df <- cbind(tdm_df, reviews$overall.ratings) # append label column from original dataset

```

**Document term matrix before removing sparse terms and after removing sparse terms**

```{r}
tdm # before removing sparse terms
```

```{r}
tdm_sparse  # after removing sparse terms
```


**Count frequency matrix VS Binary instance matrix**

```{r}
tdm_dm[1:10,] # Count frequency matrix
```
```{r}
tdm_df[1:10,] # Binary instance matrix
```

**Data splitting into train and test sets**

```{r}
s <- sample(1:nrow(tdm_df), nrow(tdm_df)*(0.70), replace = FALSE) # random sampling

train <- tdm_df[s,] # training set

test <- tdm_df[-s,] # testing set
```

```{r}
table(train$`reviews$overall.rating`) # class instances in train data
```

**Build Random forest model using repeated cross validation**

```{r}
library(caret)  # Load caret package

ctrl <- trainControl(method = "cv", number = 7) # 10-fold CV

# change to factor

train$`reviews$overall.ratings` <- as.factor(train$`reviews$overall.ratings`)
test$`reviews$overall.ratings` <- as.factor(test$`reviews$overall.ratings`)

set.seed(100)  

rf.tfidf <- train(train[,c(1:27)], train[,28],
                  method = "rpart", trControl = ctrl) # train random forest

rf.tfidf

summary(predict(rf.tfidf, train))
```

**Results**

```{r}
pred <- predict(rf.tfidf, newdata = test)  # Predicted values on test set

summary(predict(rf.tfidf, newdata = test))
```

- Random Forest 
- NN
- logistic regression
- Deep learning
- Positive/Negative. 
- multiclass classification 

```{r}
library(ranger)
library(nnet)
library(caret)
library(radiant)

set.seed(123)

# Support vector machine 
library(e1071)   


svm1 <- svm(train$'reviews$overall.ratings'~., data=train, 
          method="C-classification", kernal="radial", 
          gamma=0.1, cost=10)
summary(predict(svm1, train))

pred <- predict(svm1, newdata = test)  # Predicted values on test set

summary(pred)
xtab <- table(test$'reviews$overall.ratings', pred)
xtab

svm1_accuracy <- (1047+1525+3837+6697+7020)/nrow(test)
svm1_accuracy
```

```{r}
saveRDS(test,"test.RDS")
saveRDS(train,"train.RDS")
```

