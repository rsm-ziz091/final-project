---
title: "EDA"
author: "Ming-I Tsai"
date: "5/1/2019"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r}
library(readr)
library(tidyverse)
library(RColorBrewer)
```

##Exploratory Data Analysis
```{r}
employee_reviews <- read_csv("employee_reviews.csv")
```

###Basic Summary

####Number of reviews for each company
```{r}
count <- employee_reviews %>% group_by(company) %>% summarize(count = n())
count[7, ] = c('total', sum(count$count))
```

####Rating Distribution for each company
```{r}
rating <- employee_reviews %>% select(`company`, `overall-ratings`, `work-balance-stars`, `carrer-opportunities-stars`, `comp-benefit-stars`, `senior-mangemnet-stars`)
```

####Normalize the result
#####Overall Rating
```{r}
my_col = brewer.pal(n = 9, "BuPu")[4:9]
su <- data.frame(table(rating %>% select(company, `overall-ratings`))) %>% 
  left_join(count, by = "company") %>%
  mutate(percentage = Freq/count) %>% 
  ggplot(aes(x = factor(overall.ratings), y = percentage, group = company, fill = company)) + 
  geom_bar(stat = 'identity') + 
  facet_wrap(.~company) + 
  scale_fill_manual(values = my_col) + 
  theme(legend.position = "none") + 
  labs(title = "Overall Rating", subtitle = "In Percentage", x = "overall rating")
```
#####Work Balance
```{r}
my_col = brewer.pal(n = 9, "BuPu")[4:9]
su <- data.frame(table(rating %>% select(company, `work-balance-stars`))) %>% 
  left_join(count, by = "company") %>%
  mutate(percentage = Freq/count) %>% 
  filter(`work.balance.stars` != "none") %>%
  ggplot(aes(x = factor(`work.balance.stars`), y = percentage, group = company, 
             fill = company)) + 
  geom_bar(stat = 'identity') + 
  facet_wrap(.~company) + 
  scale_fill_manual(values = my_col) + 
  theme(legend.position = "none") + 
  labs(title = "Work Balance", subtitle = "In Percentage", x = "work balance")

su
```

#####Career Opportunities
#####Company Benefit
#####Senior Management

